import pandas as pd

from qsde import __version__
from qsde.filestore.persist import group_timeseries_by_partition
from . import get_fixture


def test_version():
    assert __version__ == "0.1.2"


def test_group_timeseries_by_partition():
    df = pd.read_csv(get_fixture("CTG_daily.csv"), parse_dates=["reference_period"])
    df_2019 = pd.read_csv(get_fixture("CTG_2019.csv"), parse_dates=["reference_period"])
    df_2020 = pd.read_csv(get_fixture("CTG_2020.csv"), parse_dates=["reference_period"])
    for name, group in group_timeseries_by_partition(df, "test", "Y"):
        if name == "2019/test.csv":
            pd.testing.assert_frame_equal(group, df_2019)
        elif name == "2020/test.csv":
            pd.testing.assert_frame_equal(group, df_2020)
        else:
            raise ValueError(f"Invalid filename = {name}")
