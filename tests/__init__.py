from pathlib import Path

def get_fixture(file_name) -> Path:
    return Path(__file__).parent / 'fixtures' / file_name